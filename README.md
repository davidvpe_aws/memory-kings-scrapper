# Memory Kings Scraper

Memory Kings Scraper is a Python-based web scraper that extracts data from the Memory Kings website (https://www.memorykings.pe). The scraper allows users to extract product information from the website and store it in a JSON or CSV file.

## Installation

To install Memory Kings Scraper, clone the repository from Github:

```
git clone
```

## Usage of Scraper ONLY

First, you need to install the required libraries using pip:

```bash
pip install -r requirements.txt
```

Now you can run the scraper with the following command, consider that there are only 51 categories in the website, if you want to scrape all of them, just remove the limit parameter. If not you can specify the number of categories you want to scrape.

```bash
python3 scraper/main.py --format=csv --limit=10
```

The output will be something like this:

```bash
File created successfully
Output in csv format is ready inside the next path: [user_path]/output_scraper/
```

## Collaborators

- David Velarde
- James Noria

## License

Memory Kings Scraper is released under the MIT License. See `LICENSE` for more information.
