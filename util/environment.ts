import * as cdk from 'aws-cdk-lib';

export function fromEnv<T extends string[]>(...vars: T) {
    type ENV = typeof vars[number];

    return vars.reduce((obj, x) => {
        if (!process.env[x]) {
            throw new Error(`Expected to receive ${x} environment variable`);
        }
        return {
            ...obj,
            [x]: process.env[x],
        };
    }, {} as { [key in ENV]: string });
}

export function fromContext<T extends string[]>(app: cdk.App, ...vars: T) {
    type ENV = typeof vars[number];

    return vars.reduce((obj, x) => {
        const value = app.node.tryGetContext(x);

        if (value === null || value === undefined) {
            throw new Error(`Expected to receive ${x} context value`);
        }

        return {
            ...obj,
            [x]: value,
        };
    }, {} as { [key in ENV]: string });
}
