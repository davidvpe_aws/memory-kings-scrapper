import { Duration, RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib'
import { BuildSpec, ComputeType, IProject, LinuxBuildImage, Project, Source } from 'aws-cdk-lib/aws-codebuild'
import { AttributeType, Table } from 'aws-cdk-lib/aws-dynamodb'
import { Rule, Schedule } from 'aws-cdk-lib/aws-events'
import { CodeBuildProject } from 'aws-cdk-lib/aws-events-targets'
import { Effect, PolicyStatement } from 'aws-cdk-lib/aws-iam'
import { LogGroup, RetentionDays } from 'aws-cdk-lib/aws-logs'
import { Bucket } from 'aws-cdk-lib/aws-s3'
import { Construct } from 'constructs'

type MemoryKingsStackProps = StackProps & {
  stage: string
  resourcePrefix: string
  sourceBucket: string
  sourceKey: string
}

export class MemoryKingsStack extends Stack {
  constructor(scope: Construct, id: string, private props: MemoryKingsStackProps) {
    super(scope, id, props)

    const { resourcePrefix, sourceKey } = props

    const artifactBucket = this.createBucket('artifacts')
    const assetsBucket = this.createBucket('assets')

    const productsTable = this.createDynamoTable()

    const sourceBucket = Bucket.fromBucketName(this, `${resourcePrefix}-source-bucket`, props.sourceBucket)

    const scrapperProjectLogGroup = new LogGroup(this, `${resourcePrefix}-scraper-project-log-group`, {
      logGroupName: `${resourcePrefix}-scraper-project-log-group`,
      removalPolicy: RemovalPolicy.DESTROY,
      retention: RetentionDays.ONE_WEEK
    })

    const scrapperProject = new Project(this, `${resourcePrefix}-scraper-project`, {
      projectName: `${resourcePrefix}-scraper-project`,
      buildSpec: BuildSpec.fromObject({
        version: 0.2,
        phases: {
          install: {
            commands: ['virtualenv venv -p python3', '. venv/bin/activate', 'pip install -r requirements.txt']
          },
          build: {
            commands: ['python --version', 'python scraper/main.py --format=json']
          },
          post_build: {
            commands: [
              `aws s3 sync ./output_scraper/ s3://${artifactBucket.bucketName}/`,
              `aws s3 sync ./images/ s3://${assetsBucket.bucketName}/`
            ]
          }
        }
      }),
      environment: {
        buildImage: LinuxBuildImage.STANDARD_6_0,
        computeType: ComputeType.SMALL,
        environmentVariables: {
          ASSETS_BUCKET_NAME: {
            value: assetsBucket.bucketName
          },
          PRODUCTS_TABLE_NAME: {
            value: productsTable.tableName
          },
          STAGE: {
            value: props.stage
          }
        }
      },
      source: Source.s3({
        bucket: sourceBucket,
        path: sourceKey
      }),
      timeout: Duration.hours(1),
      logging: {
        cloudWatch: {
          enabled: true,
          logGroup: scrapperProjectLogGroup
        }
      }
    })

    scrapperProject.addToRolePolicy(
      new PolicyStatement({
        sid: 'AllowS3ArtifactBucketAccess',
        actions: ['s3:PutObject*', 's3:GetObject*', 's3:ListBucket'],
        resources: [artifactBucket.bucketArn, `${artifactBucket.bucketArn}/*`],
        effect: Effect.ALLOW
      })
    )

    scrapperProject.addToRolePolicy(
      new PolicyStatement({
        sid: 'AllowS3AssetBucketAccess',
        actions: ['s3:PutObject*', 's3:GetObject*', 's3:ListBucket'],
        resources: [assetsBucket.bucketArn, `${assetsBucket.bucketArn}/*`],
        effect: Effect.ALLOW
      })
    )

    scrapperProject.addToRolePolicy(
      new PolicyStatement({
        sid: 'AllowDynamoTableAccess',
        actions: ['dynamodb:PutItem'],
        resources: [productsTable.tableArn],
        effect: Effect.ALLOW
      })
    )

    // Decomissioned Project - Not used anymore
    // this.scheduleProjectEveryday(scrapperProject)
  }

  createBucket(name: string) {
    const { resourcePrefix } = this.props

    const artifactBucket = new Bucket(this, `${resourcePrefix}-${name}-bucket`, {
      bucketName: `${resourcePrefix}-${name}-bucket`,
      removalPolicy: RemovalPolicy.DESTROY,
      blockPublicAccess: {
        blockPublicAcls: true,
        blockPublicPolicy: true,
        ignorePublicAcls: true,
        restrictPublicBuckets: true
      },
      versioned: true
    })

    return artifactBucket
  }

  createDynamoTable() {
    const { resourcePrefix } = this.props

    // Keys should be pk and sk to make STD (Single Table Design) possible
    const productsTable = new Table(this, `${resourcePrefix}-products-table`, {
      tableName: `${resourcePrefix}-products-table`,
      partitionKey: {
        name: 'pk',
        type: AttributeType.STRING
      },
      sortKey: {
        name: 'sk',
        type: AttributeType.STRING
      },
      removalPolicy: RemovalPolicy.DESTROY
    })

    return productsTable
  }

  scheduleProjectEveryday(project: IProject) {
    const { resourcePrefix } = this.props

    const rule = new Rule(this, `${resourcePrefix}-everyday-rule`, {
      ruleName: `${resourcePrefix}-everyday-rule`,
      schedule: Schedule.cron({ minute: '0', hour: '16', month: '*', weekDay: 'MON', year: '*' })
    })

    rule.addTarget(new CodeBuildProject(project))
  }
}
