#!/usr/bin/env node
import 'source-map-support/register'
import * as cdk from 'aws-cdk-lib'
import { MemoryKingsStack } from './memorykings-stack'
import { fromEnv } from '../util/environment'

const app = new cdk.App()

const {
  RESOURCE_PREFIX: resourcePrefix,
  SOURCE_BUCKET: sourceBucket,
  SOURCE_KEY: sourceKey,
  STAGE: stage
} = fromEnv('RESOURCE_PREFIX', 'SOURCE_BUCKET', 'SOURCE_KEY', 'STAGE')

new MemoryKingsStack(app, `${resourcePrefix}-stack`, {
  stackName: `${resourcePrefix}-stack`,
  stage,
  resourcePrefix,
  sourceBucket,
  sourceKey
})
