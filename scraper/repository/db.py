from services.dynamo import DynamoService
from models import Product
from utils import Utils


class DBRepository:
    def __init__(self, service=DynamoService()):
        self.service = service

    def _get_table_name(self):
        return Utils.get_enviroment_variable('DYNAMO_TABLE_NAME') or 'memorykings-dta-products-table'

    def add_product(self, product: Product):
        table_name = self._get_table_name()
        self.service.put_item(
            table_name, f"PRODUCT#{product.id}", f"PRODUCT#{product.id}", product.__dict__)

    def add_product_image(self, product_id: int, image_url: str):
        table_name = self._get_table_name()
        self.service.put_item(
            table_name, f"PRODUCT#{product_id}", f"IMAGE#{product_id}", {"image_url": image_url})
