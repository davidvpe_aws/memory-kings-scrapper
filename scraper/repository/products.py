from models import Product


class ProductListManager:
    def __init__(self, limit=10):
        self.product_list = []
        self.limit = limit

    def add_product(self, product):
        if (self.limit < 0 or len(self.product_list) < self.limit):
            if (self.should_add_product(product)):
                self.product_list.append(product)
                print(f"Product {len(self.product_list)} added")
            else:
                print('Product already exists, skipping...')
        else:
            print('Limit exceeded, skipping...')

    def can_add_products(self):
        return self.limit < 0 or len(self.product_list) < self.limit

    def should_add_product(self, product: Product):
        found_products = list(
            filter(lambda x: x.id == product.id, self.product_list))
        return len(found_products) == 0
