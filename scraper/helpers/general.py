import re
import os
from datetime import datetime


class Utils:

    @staticmethod
    def get_enviroment_variable(variable_name):
        return os.environ.get(variable_name)

    @staticmethod
    def get_root_path():
        return os.getcwd()

    @staticmethod
    def get_or_create_output_path(output_folder_name):
        root_path = Utils.get_root_path()
        output_folder_path = os.path.join(root_path, output_folder_name)
        if not os.path.exists(output_folder_path):
            os.makedirs(output_folder_path)
        return output_folder_path

    @staticmethod
    def get_output_file_path(output_folder_name, extension):
        output_folder_path = Utils.get_or_create_output_path(
            output_folder_name)
        filename = datetime.now().strftime('%d%m%Y-%H%M%S') + \
            '-' + os.environ.get("STAGE", "dta")
        output_file = os.path.join(
            output_folder_path, f'{filename}.{extension}')
        return output_file

    @staticmethod
    def extract_text_between(input_string):
        pattern = re.compile(r'[\n\r\n\t\xa0]')
        para_realizar_pattern = re.compile(r'Para realizar.*')
        cleaned_string = re.sub(pattern, '', input_string)
        cleaned_string = re.sub(para_realizar_pattern, '', cleaned_string)

        return cleaned_string.strip()
