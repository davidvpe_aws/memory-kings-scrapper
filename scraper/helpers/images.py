from helpers.general import Utils
import aiohttp
import aiofiles
import random
import asyncio
from os import path


class ImageDownloader(object):
    def __init__(self, client: aiohttp.ClientSession):
        print(f"Using client {client}")
        self.client = client

    async def _download_image(self, url: str, filename: str):
        try:
            async with self.client.get(url) as response:
                if response.status == 200:
                    f = await aiofiles.open(filename, mode='wb')
                    await f.write(await response.read())
                    await f.close()
        except aiohttp.ServerDisconnectedError:
            retry_ms = random.randint(500, 1500)
            print("Server disconnected, retrying in %d ms" % retry_ms)
            await asyncio.sleep(retry_ms / 1000)
            await self._download_image(url, filename)

    async def download_images(self, urls: list[str], product_id: str):
        output_path = Utils.get_or_create_output_path(f"images/{product_id}")
        tasks = []
        output_keys = []
        idx = 0
        for url in urls:
            # get extension from url
            ext = url.split(".")[-1]
            idx += 1
            filename = f"{idx}.{ext}"
            output_file_path = path.join(output_path, filename)
            output_keys.append(f"{product_id}/{filename}")
            task = asyncio.create_task(
                self._download_image(url, output_file_path))
            tasks.append(task)
        await asyncio.gather(*tasks)
        return output_keys
