from boto3 import client
from json import dumps


class DynamoService(object):
    def __init__(self, client=client('dynamodb')):

        self.client = client

    def put_item(self, table_name: str, pk: str, sk: str, item: dict):
        converted_item = self.convert(item)
        self.client.put_item(
            TableName=table_name,
            Item={
                'pk': {
                    'S': pk
                },
                'sk': {
                    'S': sk
                },
                **converted_item
            }
        )

    def convert(self, item):
        dynamo_item = {}
        for key in item.keys():
            value = item[key]
            var_type = type(value)
            if var_type == str:
                dynamo_item[key] = {"S": value}
            elif var_type == int:
                dynamo_item[key] = {"N": value}
            elif var_type == object:
                dynamo_item[key] = {"S": dumps(value)}
            else:
                raise Exception("Unsupported type: " + var_type)
        return dynamo_item
