import argparse
from memory_kings import MemoryKingsScraper
from helpers.general import Utils
import asyncio

URL_MK = 'https://www.memorykings.pe/productos'


async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--format", help="output format (csv or json)", required=True)
    parser.add_argument(
        "--limit", help="limit number of categories to scrape, consider that there are only 51", required=False, type=int, default=-1)
    parser.add_argument(
        "--output", help="output folder name", required=False, default="output_scraper"
    )
    args = parser.parse_args()

    try:
        scraper = MemoryKingsScraper(URL_MK, args.limit, args.output)

        if args.format == "csv":
            await scraper.array_to_csv()
            print(
                f"Output in csv format is ready inside the next path: {Utils.get_root_path()}/{args.output}/")
        elif args.format == "json":
            await scraper.array_to_json()
            print(
                f"Output in json format is ready inside the next path: {Utils.get_root_path()}/{args.output}/")
        else:
            print("Invalid format specified. Please choose either csv or json.")

    except UnboundLocalError:
        print("Invalid limit specified, please specify a number between 1 and 51")


if __name__ == "__main__":
    asyncio.run(main())
