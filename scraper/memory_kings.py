import random
from aiohttp import ClientSession, ClientResponseError, ServerDisconnectedError
import traceback
from bs4 import BeautifulSoup
from helpers.general import Utils
from helpers.images import ImageDownloader
import pandas as pd
import json
from models import ProductCategory, ProductList, ProductRecord, Product, MyJSONEncoder
from repository.products import ProductListManager
import asyncio

# not using requests due to https://stackoverflow.com/a/50215974


class MemoryKingsScraper:

    def __init__(self, url: str, limit, output_folder_name='output_scraper', ):
        print("Initializing MemoryKingsScraper with the following parameters:")
        print(f"URL: {url}")
        print(f"Limit: {limit}")
        print(f"Output folder name: {output_folder_name}")
        self.url = url
        self.output_folder_name = output_folder_name
        self._product_manager = ProductListManager(limit)
        self.session = ClientSession()
        print(f"Main scrapper using client {self.session}")
        self.image_downloader = ImageDownloader(self.session)

    async def get_categories(self):
        """Get categories from MemoryKings"""
        print("Getting categories from MemoryKings...")
        try:
            response = await self.session.get(self.url)
            response.raise_for_status()
            response_body = await response.text()
        except ClientResponseError as http_err:
            print(
                f'Client Response Error: {http_err}. Status: {http_err.status}')
            raise http_err
        except Exception as err:
            print(f'Other error occurred: {err}')
            raise err
        else:
            categories = BeautifulSoup(response_body, 'lxml').find_all(
                'div', class_='container pb-4')
            for category in categories:
                ul = category.find('ul')
                if ul:
                    for li in ul.find_all('li'):
                        href = li.find('div').find('a')['href']
                        full_url = 'https://www.memorykings.pe' + href
                        name = li.find('div').find('a').text
                        category = ProductCategory(name, full_url)
                        await self.get_product_listings_from_category(category)
            await self.session.close()

    async def get_product_listings_from_category(self, category: ProductCategory):
        f"""Get product lists from {category.name}"""
        if (not self._product_manager.can_add_products()):
            return

        print(f"Getting product lists from {category.name}...")
        try:
            response = await self.session.get(category.url)
            response.raise_for_status()
            response_body = await response.text()
            all_product_lists = BeautifulSoup(response_body, 'lxml').find(
                'div', class_='container pb-4').find_all('a')
            for product_list in all_product_lists:
                href = product_list['href']
                full_url = 'https://www.memorykings.pe' + href
                name = product_list.text
                product_list = ProductList(name, full_url)
                await self.get_products_from_listing(product_list)
        except ClientResponseError as http_err:
            print(f'Client Response Error: {http_err}')
            raise http_err
        except ServerDisconnectedError:
            # random wait time between 1000 and 5000 ms
            ms_to_wait = random.randint(100, 1500)
            print(
                f'========= Server Disconnected Error, will retry after {ms_to_wait} ms')
            await asyncio.sleep(ms_to_wait / 1000)
            await self.get_product_listings_from_category(category)
        except Exception as err:
            print(f'Other error occurred: {err}')
            raise err

    async def get_products_from_listing(self, product_list: ProductList):
        f"""Get products from {product_list.name}"""

        if (not self._product_manager.can_add_products()):
            return

        print(f"Getting products from {product_list.name}...")
        try:
            response = await self.session.get(product_list.url)
            response.raise_for_status()
            response_body = await response.text()
            products_container = BeautifulSoup(response_body, 'lxml').find(
                'ul', class_='products flex grid-1-4 grid-1-2-m grid-1-1-s pt-2')
            product_to_add = []

            for li in products_container.find_all('li'):
                href = li.find('div').find('a')['href']
                mk_product = 'https://www.memorykings.pe' + href
                content = li.find('div', class_='content')
                product_id = content.find(
                    'div', class_="code").find('b').text

                product_name = content.find('h4').text
                product_url = mk_product

                product_record = ProductRecord(
                    product_id, product_name, product_url)
                if (self._product_manager.should_add_product(product_record)):
                    product_to_add.append(product_record)
                else:
                    print(
                        f"Product {product_record.name} already exists, skipping...")

            await asyncio.gather(*[self.get_product_details_from_record(product) for product in product_to_add])

        except ClientResponseError as http_err:
            print(f'Client Response Error: {http_err}')
            raise http_err
        except ServerDisconnectedError:
            # random wait time between 1000 and 5000 ms
            ms_to_wait = random.randint(100, 1500)
            print(
                f'========= Server Disconnected Error, will retry after {ms_to_wait} ms')
            await asyncio.sleep(ms_to_wait / 1000)
            await self.get_products_from_listing(product_list)
        except Exception as err:
            print(
                f'Other error occurred: {err}. Traceback: {traceback.format_exc()}')
            raise err

    async def get_product_details_from_record(self, product_record: ProductRecord):
        f"""Get product details from {product_record.name}"""

        if (not self._product_manager.can_add_products()):
            return

        print(f"Getting product details from {product_record.name}...")
        try:
            response = await self.session.get(product_record.url)
            response.raise_for_status()
            response_body = await response.text()
            product_name = BeautifulSoup(response_body, 'lxml').find(
                'div', class_='container gutter-2 pt-4 car-header pb-1')
            product_detail = BeautifulSoup(response_body, 'lxml').find(
                'div', class_='container flex pb-4 grid-gutter-1 gutter-1')

            product_id = product_record.id
            product_name = product_name.find('h1').text
            product_url = product_record.url
            product_description = Utils.extract_text_between(product_detail.find(
                'div', class_='descripcion-content').text)
            images_slider_lis = product_detail.find(
                'div', class_='col-3-5 col-1-1-s pt-2').find('ul').find_all('li')

            product_images_urls = []
            for li in images_slider_lis:
                product_images_urls.append(li.find('img')['src'])

            product_price = product_detail.find(
                'div', class_='price pt-1').text
            product_properties = Utils.extract_text_between(product_detail.find(
                'div', class_='caracteristicas-content').text)

            keys = await self.image_downloader.download_images(
                product_images_urls, product_id)

            product = Product(product_id, product_name, product_url, product_description,
                              keys, product_price, product_properties)

            self._product_manager.add_product(product)

        except ClientResponseError as http_err:
            print(f'Client Response Error: {http_err}')
            raise http_err
        except ServerDisconnectedError:
            # random wait time between 1000 and 5000 ms
            ms_to_wait = random.randint(100, 1500)
            print(
                f'========= Server Disconnected Error, will retry after {ms_to_wait} ms')
            await asyncio.sleep(ms_to_wait / 1000)
            await self.get_product_details_from_record(product_record)
        except Exception as err:
            print(f'Other error occurred: {err}')
            raise err

    async def array_to_json(self):
        """Convert array to JSON"""
        output_file = Utils.get_output_file_path(
            self.output_folder_name, 'json')
        try:
            await self.get_categories()
        except Exception as err:
            print(f'Other error occurred: {err}')
            raise err
        finally:
            if (len(self._product_manager.product_list) == 0):
                print("No products found")
                return
            with open(output_file, 'w') as file:
                json.dump(self._product_manager.product_list, file,
                          ensure_ascii=False, indent=2, cls=MyJSONEncoder)
                print(f"JSON file saved to {output_file}")

    async def array_to_csv(self):
        """Convert array to CSV"""
        output_file = Utils.get_output_file_path(
            await self.output_folder_name, 'csv')
        try:
            self.get_categories()
        except Exception as err:
            print(f'Other error occurred: {err}')
            raise err
        finally:
            if (len(self._product_manager.product_list) == 0):
                print("No products found")
                return
            df = pd.DataFrame(self._product_manager.product_list)
            df.to_csv(output_file,
                      index=False, encoding='utf-8-sig')
            print(f"CSV file saved to {output_file}")
