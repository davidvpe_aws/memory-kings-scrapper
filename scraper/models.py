from json import JSONEncoder


class ProductCategory(object):
    def __init__(self, name: str, url: str):
        self.name = name.lstrip().rstrip()
        self.url = url

    def __repr__(self):
        return "ProductCategory(name=%s, url=%s)" % (self.name, self.url)


class ProductList(object):
    def __init__(self, name: str, url: str):
        self.name = name.lstrip().rstrip()
        self.url = url

    def __repr__(self):
        return "ProductList(name=%s, url=%s)" % (self.name, self.url)


class ProductRecord(object):
    def __init__(self, id: int, name: str, url: str):
        self.id = id
        self.name = name.lstrip().rstrip()
        self.url = url

    def __repr__(self):
        return "Product(name=%s, url=%s)" % (self.name, self.url)


class ProductPrice(object):
    def __init__(self, amount: str, iso: str, symbol: str):
        self.amount = amount
        self.iso = iso
        self.symbol = symbol

    def __repr__(self):
        return "ProductPrice(amount=%s, iso=%s, symbol=%s)" % (self.amount, self.iso, self.symbol)

    @staticmethod
    def from_string(price: str):
        # "price": "$ 11.00 ó S/ 41.50",

        if price is None:
            return []
        price.split(" ó ")
        prices = []
        for p in price.split(" ó "):
            symbol, amount = p.split(" ")
            iso = "PEN" if symbol == "S/" else "USD" if symbol == "$" else None
            prices.append(ProductPrice(amount, iso, symbol))
        return prices


class Product(object):
    def __init__(self, id: int, name: str, url: str, description: str, images:  list[str], price: str, properties: str):
        self.id = id
        self.name = name.lstrip().rstrip()
        self.url = url
        self.description = description
        self.images = images
        self.properties = properties
        self.prices = ProductPrice.from_string(price)

    def __repr__(self) -> str:
        return str(self.__dict__)


class MyJSONEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__
